<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DataEntry extends Model {
    protected $table = 'data_entries';

    public function getDobAttribute($value) {
        return Carbon::parse($value)->format('d/m/Y');
    }
}
