<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateDataEntry extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        // Depending on the complexity of the app requirements
        // in a real world scenario, I might do something like:
        // return $this->user()->can('create-data-entry');

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'first_name' => 'required|alpha_dash',
            'last_name'  => 'required|alpha_dash',
            'email'      => 'required|email',
            'phone'      => 'required|regex_in:^0[1-9]\d{9}$',
            'gender'     => 'required|in:female,male,other',
            'dob'        => 'required|date_format:DD/MM/YY',
            'comments'   => 'present',
        ];
    }
}
