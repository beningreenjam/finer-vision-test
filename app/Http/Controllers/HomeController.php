<?php

namespace App\Http\Controllers;

use App\DataEntry;
use Illuminate\Http\Request;

class HomeController extends Controller {
    public function index() {
        return view('home');
    }

    public function store(Request $request) {
        try {
            $entry = new DataEntry;

            $entry->first_name = $request->first_name;
            $entry->surname    = $request->surname;
            $entry->email      = $request->email;
            $entry->phone      = $request->phone;
            $entry->gender     = $request->gender;
            $entry->dob        = "{$request->dob['year']}-{$request->dob['month']}-{$request->dob['day']}";
            $entry->comments   = $request->comments;
            $entry->saveOrFail();

            $response             = new \stdClass();
            $response->first_name = $entry->first_name;
            $response->surname    = $entry->surname;

            return response()->json($response);
        }
        catch (\Exception $exception) {
            return response(
                env('production') ? 'Oops! Sorry, something has gone wrong.' : $exception->getMessage(),
                500
            );
        }
    }
}
