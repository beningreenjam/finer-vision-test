import React, {Component} from 'react';

import Step from './Step';
import Field from './Field';
import OptionField from './OptionField';

const Form = () => (
    <form method="POST" className="u-p u-border-radius u-bgcolour--lightest u-box-shadow--darkest">
        <Step title="Step 1: Your details" stepId="0">
            <div className="o-grid u-pb--x8">
                <div className="o-grid__col u-1/3">
                    <Field type="text" label="First Name" name="first_name" stepId="0"/>
                </div>
                <div className="o-grid__col u-1/3">
                    <Field type="text" label="Surname" name="surname" stepId="0"/>
                </div>
                <div className="o-grid__col u-3/3">
                    <div className="o-grid u-mt--x2">
                        <div className="o-grid__col u-1/3">
                            <Field type="email" label="Email Address" name="email" stepId="0"/>
                        </div>
                    </div>
                </div>
            </div>
        </Step>
        <Step key="step_1" title="Step 2: More comments" stepId="1">
            <div className="o-grid u-pb--x8">
                <div className="o-grid__col u-1/3">
                    <Field type="text" label="Telephone number" name="phone" stepId="1"/>
                </div>
                <div className="o-grid__col u-1/3">
                    <Field type="select" optionEmpty="Select Gender" label="Gender" name="gender" stepId="1">
                        <OptionField optionValue="female" optionLabel="Female" stepId="1" name="gender"/>
                        <OptionField optionValue="male" optionLabel="Male" stepId="1" name="gender"/>
                        <OptionField optionValue="other" optionLabel="Other" stepId="1" name="gender"/>
                    </Field>
                </div>
                <div className="o-grid__col u-3/3">
                    <div className="o-grid u-mt--x2">
                        <div className="o-grid__col u-1/4">
                            <label htmlFor="1.dob.day"><strong>Date of birth</strong></label>
                            <div className="o-grid o-grid--sm">
                                <div className="o-grid__col u-1/3">
                                    <Field type="text" name="dob.day" stepId="1"/>
                                </div>
                                <div className="o-grid__col u-1/3">
                                    <Field type="text" name="dob.month" stepId="1"/>
                                </div>
                                <div className="o-grid__col u-1/3">
                                    <Field type="text" name="dob.year" stepId="1"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Step>
        <Step title="Step 3: Final comments" stepId="2" submittable={true}>
            <div className="o-grid">
                <div className="o-grid__col u-2/3">
                    <Field type="textarea" label="Comments" name="comments" stepId="2"/>
                </div>
            </div>
        </Step>
    </form>
);

export default Form;