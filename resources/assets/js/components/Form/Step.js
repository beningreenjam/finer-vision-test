import React, {Component} from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import moment from 'moment';
import validate from 'validate.js';

import rules from './validation-rules';
import {setStep, setFieldErrors} from '../../actions';

validate.extend(validate.validators.datetime, {
    parse : function (value) {
        return +moment.utc(value);
    },
    format: function (value) {
        return moment.utc(value).format('YYYY-MM-DD');
    }
});

class Step extends Component {
    constructor(props) {
        super(props);
        
        this.validateStep = this.validateStep.bind(this);
        this.switchToStep = this.switchToStep.bind(this);
        this.nextStep     = this.nextStep.bind(this);
        this.submit       = this.submit.bind(this);
    }
    
    switchToStep() {
        const {stepId, setStep} = this.props;
        setStep(parseInt(stepId));
    }
    
    validateStep(event) {
        event.preventDefault();
        let {stepId, values, submittable = false, setFieldErrors} = this.props;
        
        const errors = validate(values[stepId], rules[stepId]);
        const failed = (typeof errors == 'object');
        
        setFieldErrors(stepId, failed ? errors : {});
        
        if (!failed) {
            submittable ? this.submit() : this.nextStep();
        }
        else {
            alert(Object.values(errors).join('\n\n'));
        }
    }
    
    nextStep() {
        const {stepId, setStep} = this.props;
        setStep(parseInt(stepId) + 1);
    }
    
    submit() {
        let data = {};
        
        for (const section of this.props.values) {
            Object.assign(data, section);
        }
        
        axios.post('/', data)
             .then(response => {
                 const {first_name, surname} = response.data;
                 alert(`${first_name} ${surname} saved successfully.`);
             })
             .catch(error => {
                 if(typeof error.response == 'object') {
                     alert(error.response.data);
                 }
             });
    }
    
    render() {
        const {title, stepId, step, submittable = false, children} = this.props;
        
        return (
            <fieldset className={`u-bgcolour--grey u-border-radius${submittable ? '' : ' u-mb'}`}>
                <legend onClick={this.switchToStep}
                        className="u-p--x3 u-border-radius u-gradient--secondary u-inner-shadow--lightest u-colour--lightest u-text-shadow--dark u-cursor--pointer u-font--heading">{title}</legend>
                <div className={`c-input-group c-input-group--${step == stepId ? 'expanded' : 'collapsed'}`}>
                    <div className="u-p--x2 u-cf">
                        <div className="u-position-relative">
                            {children}
                            <button type='button' onClick={this.validateStep}
                                    className="u-pv--x2 u-ph--x8 u-border-radius u-colour--lightest u-gradient--tertiary u-text-shadow--dark u-inner-shadow--lightest u-position-absolute u-position-bottom-right o-type--x2">
                                Next&nbsp;&nbsp;&gt;
                            </button>
                        </div>
                    </div>
                </div>
            </fieldset>
        )
    }
}

const mapStateToProps = ({step, values, errors}) => ({step, values, errors});

const mapDispatchToProps = dispatch => ({
    setStep       : (step) => dispatch(setStep(step)),
    setFieldErrors: (stepId, errors) => dispatch(setFieldErrors({stepId, errors})),
});

export default connect(mapStateToProps, mapDispatchToProps)(Step);