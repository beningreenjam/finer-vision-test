import React, {Component} from 'react';
import {connect} from 'react-redux';

import {setFieldValue} from '../../actions';

class Field extends Component {
    constructor(props) {
        super(props);
        
        this.handleChange = this.handleChange.bind(this);
    }
    
    handleChange(event) {
        let {name, value} = event.target;
        this.props.setFieldValue(name, value);
    }
    
    render() {
        let {label, type, name, stepId, values, optionEmpty = 'Unselected', children} = this.props;
        
        name = name.split('.');
        
        const textValue = (name.length == 1) ? values[stepId][name[0]] : values[stepId][name[0]][name[1]];
        const textName  = (name.length == 1) ? `${stepId}.${name[0]}` : `${stepId}.${name[0]}.${name[1]}`;
        
        optionEmpty = (textValue.length > 0) ? '' : optionEmpty;
        
        return (
            <div>
                {
                    typeof label != undefined &&
                    <label htmlFor={textName} className="u-display-block u-mb"><strong>{label}</strong></label>
                }
                {
                    !!~['text', 'email'].indexOf(type) &&
                    <input type={type} id={textName} name={textName} value={textValue} onChange={this.handleChange} autoComplete="off"
                           className="u-inner-shadow--darkest u-border-radius u-p--x2"/>
                }
                {
                    type == 'select' &&
                    <div className="c-select u-inner-shadow--darkest u-gradient--darker u-colour--lightest u-border-radius">
                        <input type="text" readOnly="readOnly" className="u-border-radius"/>
                        <div className="c-select__list" data-empty={optionEmpty}>{children}</div>
                    </div>
                }
                {
                    type == 'textarea' &&
                    <textarea id={textName} name={textName} value={textValue}
                              onChange={this.handleChange} className="u-inner-shadow--darkest u-border-radius u-ph--x3 u-pv--x2"/>
                }
            </div>
        )
    }
}

const mapStateToProps = ({values}) => ({values});

const mapDispatchToProps = dispatch => ({
    setFieldValue: (name, value) => dispatch(setFieldValue({name, value})),
});

export default connect(mapStateToProps, mapDispatchToProps)(Field);