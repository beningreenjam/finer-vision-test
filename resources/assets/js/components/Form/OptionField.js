import React, {Component} from 'react';
import {connect} from 'react-redux';

import {setFieldValue} from '../../actions';

class OptionField extends Component {
    constructor(props) {
        super(props);
        
        this.handleChange = this.handleChange.bind(this);
    }
    
    handleChange(event) {
        let {name, value} = event.target;
        this.props.setFieldValue(name, value);
    }
    
    render() {
        let {label, name, stepId, values, optionLabel = '', optionValue = ''} = this.props;
        
        const textValue = values[stepId][name];
        const textName  = `${stepId}.${name}`;
        
        return (
            <label>
                <input type="radio" name={textName} value={optionValue} checked={textValue == optionValue} onChange={this.handleChange}/>
                <span data-value={optionLabel} className="u-ph--x3 u-pv--x2 u-bgcolour--lightest u-colour--dark">{optionLabel}</span>
            </label>
        )
    }
}

const mapStateToProps = ({values}) => ({values});

const mapDispatchToProps = dispatch => ({
    setFieldValue: (name, value) => dispatch(setFieldValue({name, value})),
});

export default connect(mapStateToProps, mapDispatchToProps)(OptionField);