export default [
    {
        first_name: {
            presence: {
                allowEmpty: false
            },
            length  : {
                minimum: 2
            }
        },
        surname : {
            presence: {
                allowEmpty: false
            },
            length  : {
                minimum: 2
            }
        },
        email     : {
            presence: {
                allowEmpty: false
            },
            email   : true
        },
    },
    {
        phone      : {
            length      : {
                is: 11
            },
            numericality: {
                onlyInteger: true,
                greaterThan: 0
            },
            format      : {
                pattern: /^0[1-9]\d{9}$/,
            }
        },
        gender     : {
            presence : {
                allowEmpty: false,
                message   : "^Please select your gender from the list"
            },
            inclusion: ['female', 'male', 'other']
        },
        'dob.day'  : {
            length      : {
                is: 2
            },
            numericality: {
                onlyInteger      : true,
                greaterThan      : 0,
                lessThanOrEqualTo: 31
            }
        },
        'dob.month': {
            length      : {
                is: 2
            },
            numericality: {
                onlyInteger      : true,
                greaterThan      : 0,
                lessThanOrEqualTo: 12
            }
        },
        'dob.year' : {
            length      : {
                is: 4
            },
            numericality: {
                onlyInteger: true
            }
        },
        'dob.full' : {
            datetime: {
                dateOnly: true,
                message : "^Date of birth is not a valid date"
            }
        },
    },
    {
        comments: {
            presence: true
        }
    }
]