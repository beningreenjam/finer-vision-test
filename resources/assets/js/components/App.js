import React, {Component} from 'react';
import {Route} from 'react-router-dom';

import Form from './Form/';

const App = () => (
    <div>
        <Route exact path='/' component={Form}/>
    </div>
);

export default App;