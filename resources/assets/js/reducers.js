export const step = (state, {type, data}) => {
    switch (type) {
        case "SET_CURRENT_STEP":
            data = parseInt(data);
            if (isNaN(data) || data < 0 || data > 2) return state;
            
            return data;
        default:
            return state || 0;
    }
};

export const values = (state, {type, data}) => {
    switch (type) {
        case "SET_FIELD_VALUE":
            let {name, value} = data;
            
            name = name.split('.');
            if (name.length < 2 || name.length > 3) return state;
            
            let sectionKey = name[0];
            let fieldKey   = name[1];
            if (typeof state[sectionKey] == undefined || !state[sectionKey].hasOwnProperty(fieldKey)) return state;
            
            return state.map((obj, prop) => {
                if (prop != sectionKey) return obj;
                
                const newValue = name.length == 2 ?
                                 value : Object.assign({}, state[sectionKey][fieldKey], {[name[2]]: value});
                
                if (fieldKey == 'dob') {
                    newValue.full = `${newValue.year}-${newValue.month}-${newValue.day}`;
                }
                
                return Object.assign({}, state[sectionKey], {
                    [fieldKey]: newValue
                });
            });
        default:
            return state || [
                    {
                        first_name: '',
                        surname : '',
                        email     : '',
                    },
                    {
                        phone : '',
                        gender: '',
                        dob   : {
                            day  : '',
                            month: '',
                            year : '',
                            full : '',
                        },
                    },
                    {
                        comments: '',
                    }
                ]
    }
};

export const errors = (state, {type, data}) => {
    switch (type) {
        case "SET_FIELD_ERRORS":
            let {stepId, errors} = data,
                newState         = state;
            
            newState[stepId] = errors;
            
            return newState;
        default:
            return state || [];
    }
};