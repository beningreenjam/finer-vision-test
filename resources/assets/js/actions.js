export const setStep = (data) => ({type: "SET_CURRENT_STEP", data});
export const setFieldValue = (data) => ({type: "SET_FIELD_VALUE", data});
export const setFieldErrors = (data) => ({type: "SET_FIELD_ERRORS", data});