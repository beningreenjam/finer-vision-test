<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>@yield('head.title', 'Finer Vision Data Entry Form Test')</title>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    @section('head.meta')
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    @show
</head>
<body class="u-bgcolour--primary u-font--body u-vertical-center">
    <div id="root" class="o-container">@yield('react-components')</div>

    @section('scripts')
        <script src="{{ mix('js/app.js') }}"></script>
    @show
</body>
</html>
