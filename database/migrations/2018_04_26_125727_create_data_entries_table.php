<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataEntriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('data_entries', function (Blueprint $table) {
			$table->increments('id');
			$table->string('first_name');
			$table->string('surname');
			$table->string('email')->index();
			$table->string('phone')->index();
			$table->enum('gender', ['female', 'male', 'other']);
			$table->date('dob')->index();
			$table->text('comments')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('data_entries');
	}
}
