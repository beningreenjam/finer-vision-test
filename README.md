# Finer Vision Test

## Prerequisites
The instructions below are based on my personal setup of a Mac OS environment with Laravel Valet and a MySQL database setup.

## Instructions
Run `npm install && composer install`

Enter your database details in your `.env` file.

Run `php artisan migrate:install && php artisan migrate && valet link && npm run prod && valet open`